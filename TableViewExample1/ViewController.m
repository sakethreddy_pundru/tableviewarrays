//
//  ViewController.m
//  TableViewExample1
//
//  Created by admin on 02/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController{
    NSMutableArray *mainarray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    mainarray = [[NSMutableArray alloc]init];
    NSMutableArray *array1 = [[NSMutableArray alloc]initWithObjects:@"WiFi",@"Bluetooth",@"SIM cards",@"Data usage",@"More",nil];
    NSMutableArray *array2 = [[NSMutableArray alloc]initWithObjects:@"Home",@"Display",@"Sound & notification",@"storage",@"battery",@"Apps",@"users", nil];
    NSMutableArray *array3 = [[NSMutableArray alloc]initWithObjects:@"Location",@"Security",@"Accounts", nil];
    [mainarray addObject:array1];
    [mainarray addObject:array2];
    [mainarray addObject:array3];
    
    UITableView *tableview = [[UITableView alloc]init];
    tableview.frame = self.view.frame;
    tableview.delegate = self;
    tableview.dataSource=self;
    [self.view addSubview:tableview];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [mainarray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray *currentarray = [mainarray objectAtIndex:section];
    return [currentarray count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return @"Wireless & Networks";
    
    }else if(section==1){
        return @"Device";
    
    }else{
        return @"Personal";
    }
}


- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
   
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor lightGrayColor]];
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *innerarray= [mainarray objectAtIndex:indexPath.section];
//    static NSString *cellidentifier = @"cellid";
    NSString *value = [innerarray objectAtIndex:indexPath.row];
    

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:value];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:value];
    }
    cell.textLabel.text = [innerarray objectAtIndex:indexPath.row];
    UILabel *label = [[UILabel alloc]init];
    cell.textLabel.font = [UIFont systemFontOfSize:12.0];

    label.text = value;
    [cell addSubview:label];
    return cell;
    }


    
   - (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
